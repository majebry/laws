<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->as('admin.')->namespace('Admin')->middleware('auth')->group(function () {
    Route::resource('types', 'TypeController');
    Route::resource('specialities', 'SpecialityController');
    Route::resource('texts', 'TextController');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/', 'TextController@index');
Route::get('{year}/{number}', 'TextController@show');
