<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function text ()
    {
        return $this->hasMany('App\Text');
    }
}
