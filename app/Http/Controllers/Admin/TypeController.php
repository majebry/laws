<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Type;

class TypeController extends Controller
{
    public function index()
    {
        $types = Type::all();

        return view('admin.types.index', compact('types'));
    }

    public function create()
    {
        return view('admin.types.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  =>  'required'
        ]);

        Type::create($request->all());

        return redirect()->route('admin.types.index');
    }
}
