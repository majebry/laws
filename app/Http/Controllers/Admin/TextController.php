<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Text;

class TextController extends Controller
{
    public function index()
    {
        $texts = Text::all();

        return view('admin.texts.index', compact('texts'));
    }

    public function create()
    {
        return view('admin.texts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'number'    =>  'required|numeric',
            'year'      =>  'required|numeric',
            'type_id'   =>  'required|exists:types,id',
            'speciality_id'   =>  'required|exists:specialities,id',
            'body'      =>  'required',
        ]);

        Text::create($request->all());

        return redirect()->route('admin.texts.index');
    }
}
