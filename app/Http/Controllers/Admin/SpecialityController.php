<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Speciality;

class SpecialityController extends Controller
{
    public function index()
    {
        $specialities = Speciality::all();

        return view('admin.specialities.index', compact('specialities'));
    }

    public function create()
    {
        return view('admin.specialities.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  =>  'required'
        ]);

        Speciality::create($request->all());

        return redirect()->route('admin.specialities.index');
    }
}
