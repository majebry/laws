<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Text;

class TextController extends Controller
{
    public function index(Request $request)
    {
        $texts = Text::query();

        if ($request->keyword) {
            $texts->where('body', 'LIKE', '%' . $request->keyword . '%');
        }

        if ($request->number) {
            $texts->where('number', $request->number);
        }

        if ($request->year_from) {
            $texts->where('year', '>', $request->year_from);
        }

        if ($request->year_until) {
            $texts->where('year', '<', $request->year_until);
        }

        if ($request->type_id) {
            $texts->where('type_id', $request->type_id);
        }

        if ($request->speciality_id) {
            $texts->where('speciality_id', $request->speciality_id);
        }

        $texts = $texts->get();

        return view('texts.index', compact('texts'));
    }

    public function show($year, $number)
    {
        $text = Text::where(compact('year', 'number'))->firstOrFail();

        return view('texts.show', compact('text'));
    }
}
