<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $fillable = [
        'type_id', 'number', 'year', 'body', 'speciality_id'
    ];

    public $timestamps = false;

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function speciality()
    {
        return $this->belongsTo('App\Speciality');
    }
}
