<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function text ()
    {
        return $this->hasMany('App\Text');
    }
}
