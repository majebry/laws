@extends('layouts.home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <form class="form-inline" action="" method="get">
                    <div class="panel-body">
                        <div class="form-group">
                            <input type="text" name="keyword" class="form-control" value="{{ request()->keyword }}" placeholder="ابحث بكلمة">

                            <input id="number" type="number" class="form-control" name="number" value="{{ request()->number }}" autofocus placeholder="رقم ">

                            <select class="form-control" name="year_from">
                                <option value="" selected disabled>-- من السنة --</option>
                                <option value="">غير محدد</option>
                                @foreach (range(date('Y'), 1900) as $year)
                                    <option value="{{ $year }}" @if ($year == request()->year_from))
                                        selected
                                    @endif>{{ $year }}</option>
                                @endforeach
                            </select>

                            <select class="form-control" name="year_until">
                                <option value="" selected disabled>-- إلى السنة --</option>
                                <option value="">غير محدد</option>
                                @foreach (range(date('Y'), 1900) as $year)
                                    <option value="{{ $year }}" @if ($year == request()->year_until))
                                        selected
                                    @endif>{{ $year }}</option>
                                @endforeach
                            </select>

                            <select class="form-control" name="type_id">
                                <option value="" selected>-- جميع الأصناف  --</option>
                                @foreach ($types as $type)
                                    <option value="{{ $type->id }}" @if ($type->id == request()->type_id))
                                        selected
                                    @endif>{{ $type->name }}</option>
                                @endforeach
                            </select>

                            <select class="form-control" name="speciality_id">
                                <option value="" selected>-- جميع التخصصات --</option>
                                @foreach ($specialities as $speciality)
                                    <option value="{{ $speciality->id }}" @if ($speciality->id == request()->speciality_id))
                                        selected
                                    @endif>{{ $speciality->name }}</option>
                                @endforeach
                            </select>

                            <input type="submit" name="" value="فلتر" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>

            @if (count($texts))
                <div class="panel panel-default">
                    <div class="panel-heading">النصوص</div>

                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>العنوان</th>
                                        <th>التخصص</th>
                                        <th>خيارات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($texts as $text)
                                        <tr>
                                            <td>{{ $text->id }}</td>
                                            <td>{{ $text->type->name }} رقم {{ $text->number }} لسنة {{ $text->year }}</td>
                                            <td>{{ $text->speciality->name }}</td>
                                            <td>
                                                <a class="btn btn-info" href="{{ url($text->year . '/' . $text->number . '?keyword=' . request()->keyword) }}">قراءة</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                </div>
            @else
                <div class="alert alert-warning">
                    <p>عفواً، لا يوجد نتائج لبحثك!</p>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
