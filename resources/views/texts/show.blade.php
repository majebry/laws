@extends('layouts.home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if ($text)
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $text->type->name }} رقم {{ $text->number }} لسنة {{ $text->year }}</div>

                        <div class="panel-body">
                            {!! str_replace(request()->keyword, '&#x200d;<span style="background-color:yellow;">' . request()->keyword . '&#x200d;</span>', nl2br($text->body)) !!}
                        </div>

                </div>
            @endif
        </div>
    </div>
</div>
@endsection
