@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group text-left">
                <a class="btn btn-primary" href="{{ route('admin.specialities.create') }}">إضافة تخصص</a>
            </div>

            @if (count($specialities))
                <div class="panel panel-default">
                    <div class="panel-heading">التخصصات</div>

                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم الصنف</th>
                                        <th>خيارات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($specialities as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>...</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                </div>
            @endif
        </div>
    </div>
</div>
@endsection
