@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group text-left">
                <a class="btn btn-primary" href="{{ route('admin.types.create') }}">إضافة صنف</a>
            </div>

            @if (count($types))
                <div class="panel panel-default">
                    <div class="panel-heading">الأصناف</div>

                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم الصنف</th>
                                        <th>خيارات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($types as $type)
                                        <tr>
                                            <td>{{ $type->id }}</td>
                                            <td>{{ $type->name }}</td>
                                            <td>...</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                </div>
            @endif
        </div>
    </div>
</div>
@endsection
