@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">إضافة نص</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('admin.texts.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                            <label for="number" class="col-md-3 control-label">رقم</label>

                            <div class="col-md-7">
                                <input id="number" type="number" class="form-control" name="number" value="{{ old('number') }}" required autofocus>

                                @if ($errors->has('number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                            <label for="year" class="col-md-3 control-label">سنة الإصدار</label>

                            <div class="col-md-7">
                                <select class="form-control" name="year">
                                    <option value="" selected disabled>-- سنة الإصدار --</option>
                                    @foreach (range(date('Y'), 1900) as $year)
                                        <option value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('year'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('year') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('type_id') ? ' has-error' : '' }}">
                            <label for="type_id" class="col-md-3 control-label">الصنف</label>

                            <div class="col-md-7">
                                <select class="form-control" name="type_id">
                                    <option value="" disabled selected>-- اختر صنف --</option>
                                    @foreach ($types as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('type_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('speciality_id') ? ' has-error' : '' }}">
                            <label for="speciality_id" class="col-md-3 control-label">التخصص</label>

                            <div class="col-md-7">
                                <select class="form-control" name="speciality_id">
                                    <option value="" disabled selected>-- اختر تخصص --</option>
                                    @foreach ($specialities as $speciality)
                                        <option value="{{ $speciality->id }}">{{ $speciality->name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('speciality_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('speciality_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label for="body" class="col-md-3 control-label">المحتوى</label>

                            <div class="col-md-7">
                                <textarea name="body" rows="20" class="form-control">{{ old('body') }}</textarea>

                                @if ($errors->has('body'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-7 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    إضافة
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
