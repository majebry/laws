@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group text-left">
                <a class="btn btn-primary" href="{{ route('admin.texts.create') }}">إضافة نص</a>
            </div>

            @if (count($texts))
                <div class="panel panel-default">
                    <div class="panel-heading">النصوص</div>

                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>العنوان</th>
                                        <th>التخصص</th>
                                        <th>خيارات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($texts as $text)
                                        <tr>
                                            <td>{{ $text->id }}</td>
                                            <td>{{ $text->type->name }} رقم {{ $text->number }} لسنة {{ $text->year }}</td>
                                            <td>{{ $text->speciality->name }}</td>
                                            <td>...</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                </div>
            @endif
        </div>
    </div>
</div>
@endsection
